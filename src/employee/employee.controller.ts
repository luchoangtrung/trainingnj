import { Controller, Get, Res, HttpStatus, Param, NotFoundException, Post, Body, Query, Put, Delete } from '@nestjs/common';
import { EmployeeService } from './employee.service';
import { CreateEmployeeDTO } from './dto/create-employee.dto';
import { ValidateObjectId } from '../shared/pipes/validate-object-id.pipes';
const Web3 = require('web3')

const net = 'https://ropsten.infura.io/v3/7015ab62806c43e282a78fd60aa59fdd';
const web3 = new Web3(net);

@Controller('company')
export class EmployeeController {
    constructor(private employeeService: EmployeeService) { }

    @Get('balance/:walletAddress')
    async getBalance(@Res() res, @Param('walletAddress') walletAddress) {
        const balance = await this.employeeService.getBalance(walletAddress);
        if (!balance) throw new NotFoundException('Employee does not exist!');
        return res.status(HttpStatus.OK).json(balance);
    }

    @Get('employeeList')
    async getEmployeeList(@Res() res) {
        const employeeList = [];

        await this.employeeService.getEmployeeList().then(async function (response){
            
            await response.forEach(async function(element){
                console.log(element.WalletAddress);
                const balance = await web3.eth.getBalance(element.WalletAddress);
                console.log(balance);
                element.Balance = await web3.eth.getBalance(element.WalletAddress);
                console.log(element);
                employeeList.push(element);
                
                if(employeeList.length == response.length)
                    return res.status(HttpStatus.OK).json(employeeList);
            });
        });
    }

    @Get('employee/:employeeID')
    async getEmployee(@Res() res, @Param('employeeID', new ValidateObjectId()) employeeID) {
        const empl = await this.employeeService.getEmployee(employeeID);
        const balance = await web3.eth.getBalance(empl.WalletAddress);
        
        empl.Balance = (Number(balance) / Math.pow(10,18)).toString();
        await console.log(empl);
        return res.status(HttpStatus.OK).json(empl);
    }

    @Post('/employee')
    async addEmployee(@Res() res, @Body() createEmployeeDTO: CreateEmployeeDTO) {
        createEmployeeDTO.WalletAddress = "0x310dcabc84dC0CE2A67d3943Fe4B01b2e2CA6DF2";
        const newEmployee = await this.employeeService.addEmployee(createEmployeeDTO);
        return res.status(HttpStatus.OK).json({
            message: "Employee has been submitted successfully!",
            employee: newEmployee
        })
    }

    @Put('/edit')
    async editEmployee(
        @Res() res,
        @Query('employeeID', new ValidateObjectId()) employeeID,
        @Body() createEmployeeDTO: CreateEmployeeDTO
    ) {
        const editedEmployee = await this.employeeService.editEmployee(employeeID, createEmployeeDTO);
        if (!editedEmployee) throw new NotFoundException('Employee does not exist!');
        return res.status(HttpStatus.OK).json({
            message: 'Employee has been successfully updated',
            employee: editedEmployee
        })
    }

    @Delete('/delete')
    async deleteEmployee(@Res() res, @Query('employeeID', new ValidateObjectId()) employeeID) {
        const deletedEmployee = await this.employeeService.deleteEmployee(employeeID);
        if (!deletedEmployee) throw new NotFoundException('Employee does not exist!');
        return res.status(HttpStatus.OK).json({
            message: 'Employee has been deleted!',
            employee: deletedEmployee
        })
    }
}
