export class CreateEmployeeDTO {
    readonly Name: string;
    WalletAddress: string;
    readonly Balance: string;
    readonly Address: string;
    readonly Gender: boolean;
    readonly Age: number;
}