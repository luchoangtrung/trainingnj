import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Employee } from './interfaces/employee.interface';
import { CreateEmployeeDTO } from './dto/create-employee.dto';

const Web3 = require('web3')

@Injectable()
export class EmployeeService {
    constructor(@InjectModel('Employee') private readonly employeeModel: Model<Employee>) { }

    async getBalance(walletAddress): Promise<string> {
         const web3 = new Web3(Web3.givenProvider || 'HTTP://127.0.0.1:7545', null, {});
        //const web3 = new Web3(Web3.givenProvider || new Web3.providers.WebsocketProvider('HTTP://127.0.0.1:7545'), null, {});
        const balance = web3.eth.getBalance(walletAddress);
        return balance;
    }

    async getEmployeeList(): Promise<Employee[]> {
        const employeeList = await this.employeeModel.find().exec();
        return employeeList;
    }

    async getEmployee(employeeID): Promise<Employee> {
        const employee = await this.employeeModel
            .findById(employeeID)
            .exec();
        return employee;
    }

    async addEmployee(createEmployeeDTO: CreateEmployeeDTO): Promise<Employee> {
        const newEmployee = await this.employeeModel(createEmployeeDTO);
        return newEmployee.save();
    }

    async editEmployee(employeeID, createEmployeeDTO: CreateEmployeeDTO): Promise<Employee> {
        const editedEmployee = await this.employeeModel
            .findByIdAndUpdate(employeeID, createEmployeeDTO, { new: true });
        return editedEmployee;
    }

    async deleteEmployee(employeeID): Promise<any> {
        const deletedEmployee = await this.employeeModel
            .findByIdAndRemove(employeeID);
        return deletedEmployee;
    }

}
