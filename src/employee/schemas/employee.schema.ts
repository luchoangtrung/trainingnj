import * as mongoose from 'mongoose';

export const EmployeeSchema = new mongoose.Schema({
    Name: String,
    WalletAddress: String,
    Balance: String,
    Address: String,
    Gender: Boolean,
    Age: Number
})