import { Document } from 'mongoose';

export interface Employee extends Document {
    readonly Name: string;
    readonly WalletAddress: string;
    Balance: string;
    readonly Address: string;
    readonly Gender: boolean;
    readonly Age: number
}